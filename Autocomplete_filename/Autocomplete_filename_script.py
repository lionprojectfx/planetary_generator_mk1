import os
from PIL import Image
import datetime

COLOR_NAMES = {
    (255,0,0):'Красный',
    (0,255,0):'Зеленый',
    (0,0,255):"Синий",
    (255,255,0):"Жёлетый",
    (0,255,255):"Аква",
    (255,0,255):"Пурпурный",
}
def get_primary_color(image_path):
    image = Image.open(image_path)
    image = image.convert("RGB")
    width, height = image.size
    pixels = image.getcolors(width * height)
    sorted_pixels = sorted(pixels, key=lambda t: t[0], reverse=True)
    primary_color = sorted_pixels[0][1]
    closest_color = find_closest_color(primary_color)
    return closest_color
def find_closest_color(rgb):
    min_distance = float("inf")
    closest_color = None
    for color, name in COLOR_NAMES.items():
        distance = calculate_distance(rgb, color)
        if distance < min_distance:
            min_distance = distance
            closest_color = name
    return closest_color
def calculate_distance(rgb1, rgb2):
    r1, g1, b1 = rgb1
    r2, g2, b2 = rgb2
    return ((r1 - r2) ** 2 + (g1 - g2) ** 2 + (b1 - b2) ** 2) ** 0.5
def rename_files(folder_path, new_name):
    file_list = os.listdir(folder_path)
    for index, file_name in enumerate(file_list):
        file_path = os.path.join(folder_path, file_name)
        if os.path.isfile(file_path):
            file_ext = os.path.splitext(file_name)[1]
            if file_ext.lower() in ['.jpg', '.jpeg', '.png', '.gif','jfif']: # Можно добавить отсутствующий формат...
                primary_color = get_primary_color(file_path)
                creation_time = os.path.getctime(file_path)
                creation_date = datetime.datetime.fromtimestamp(creation_time)
                creation_date_str = creation_date.strftime("%d.%m.%Y %H.%M")
                new_file_name = f"({primary_color}_спектр)_{new_name}_{index + 1}_{creation_date_str}{file_ext}"
                new_file_path = os.path.join(folder_path, new_file_name)
                os.rename(file_path, new_file_path)

# Вводимые данные
folder_path = input('Введите путь к папке, в которой необходимо переписать файлы/изображения...')
new_name = input('Введите название файла, для разделения используйте знак "_"...')
rename_files(folder_path, new_name)