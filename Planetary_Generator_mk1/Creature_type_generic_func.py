import random
def creature_type():
    list1 = ['Protozoa', 'Sponges', 'Coleoptera', 'Worms', 'Molluscs', 'Arthropods', 'Chordates']
    list2 = ['Crustaceans', 'Spiders', 'Insects']
    list3 = ['Mammals', 'Birds', 'Amphibians', 'Fish', 'Reptiles']

    key = random.choice(list1)
    value = ''

    if key == 'Arthropods':
        value = random.choice(list2)
    elif key == 'Chordates':
        value = random.choice(list3)
    else:
        value = 'Unknown'

    creature_type = {key:value}

    text = '\n'.join(f'Class {key}, Subtype {value}' for key, value in creature_type.items())

    return text

