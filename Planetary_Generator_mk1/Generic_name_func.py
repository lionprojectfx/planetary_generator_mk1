import random

def text_generic():

    with open("Name_box.txt", "r") as file:
        names = file.read().split(",")

    # Generate objects
    objects = []
    for _ in range(100):
        name = random.choice(names).strip()
        letters = random.choices('ABCDEFGHIJKLMNOPQRSTUVWXYZ', k=2)
        numbers = random.choices(range(10), k=3)
        id_numbers = random.choices(range(10), k=random.randint(5, 15))

        obj = f"{name}_{letters[0]}{letters[1]}{''.join(map(str, numbers))}_id_{''.join(map(str, id_numbers))}"
        objects.append(obj)

    # Write objects to a file
    filename = f"Name_gen.txt"
    with open(filename, "w") as file:
        file.write(",".join(objects))

