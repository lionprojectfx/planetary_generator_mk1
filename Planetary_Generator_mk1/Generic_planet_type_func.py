import random
from Creature_type_generic_func import creature_type # Импорт скрипта для создания типа обитателей планеты

def planet_generation():
    def generate_type(value):
        if 1000 <= value <= 4000:
            types = ["Dwarf planet", "Satellite", "Gas dwarf", "Coreless planet", "Silicate planet",
                     "Deformed planet", "Iron planet", "Hycean planet", "Ocean planet", "Stone planet",
                     "Lava planet", "Protoplanet"]
        elif 4000 <= value <= 25000:
            types = ["Desert planet", "Ice planet", "Lava planet", "Earth-type planet", "Giant satellite",
                     "Carbon planet", "Silicate planet", "Mountain planet", "Ocean planet", "Iron planet"]
        elif 25000 <= value <= 150000:
            types = ["Gas Giant", "Super Planet", "Helium Planet"]
        else:
            types = ["Unknown"]

        return random.choice(types)


    # Создание базовых параметров планеты
    objects = []
    for _ in range(100):
        value = random.randint(1000, 150000)
        obj_type = generate_type(value)

        obj = f"{obj_type}. Average diameter: {value} kilometers"
        objects.append(obj)

    # print(objects) # Тестовый вывод №1

    # Теперь начинаем процедуру фильтрации
    # Lifeless_planets - это те, что не подходят для добавления записи о присутствии жизни
    # Suitable_planets_for_life - в этот список попадают те планеты, что подходят для жизни

    Suitable_planets_for_life = []
    Lifeless_planets = []

    for obj in objects:
        start_index = obj.find(":")
        end_index = obj.find(" kilometers")
        diameter = int(obj[start_index + 1: end_index])
        if 4000 <= diameter <= 25000:
            Suitable_planets_for_life.append(obj)
        else:
            Lifeless_planets .append(obj)

    objects = Lifeless_planets

    # В этой части кода создается список "Populated_planets" из тех планет из списка inhabited_planets_1, которые БУДУТ иметь жизнь

    Populated_planets = []

    for obj in Suitable_planets_for_life:
        if random.randint(1, 4) == 1:
            intelligent_life = random.choice(["present", "absent"])
            num_living_creatures = random.randint(1, 10000000000)
            species_diversity = random.choice(["huge", "large", "medium", "small", "negligible"])
            settlements = "present" if intelligent_life == "present" and random.randint(1, 2) == 1 else "absent"
            dominance_percentage = random.randint(10,95)

            Populated_planets.append(f"{obj} The planet is inhabited. Intelligent life forms are {intelligent_life}. "
                                       f"The number of living creatures on the planet: {num_living_creatures}. "
                                       f"The most common type of living beings: {creature_type()}. Dominance factor: {dominance_percentage}%. " # Здесь используется скрипт генерика для типов живых существ
                                       f"Species diversity: {species_diversity}. Settlements: {settlements}.")

    return Populated_planets,Lifeless_planets

    # Для использования в основной сборке нужны такие значения, как Populated_planet[0] и Lifeless_planets[1]



