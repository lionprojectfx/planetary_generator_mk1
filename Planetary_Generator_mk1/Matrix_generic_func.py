import random

def matrix_gen():
    def generate_character_matrix():
        matrix = []
        for _ in range(100):
            row = [str(random.randint(0, 9)) for _ in range(100)]
            matrix.append(row)
        return matrix

    # Generate the character matrix
    char_matrix = generate_character_matrix()

    # Convert the character matrix to a string representation
    matrix_str = '\n'.join([' '.join(row) for row in char_matrix])

    return matrix_str

