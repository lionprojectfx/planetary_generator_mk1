from Generic_planet_type_func import *
from Matrix_generic_func import *
import random
import json

Populated_planets = planet_generation()
Lifeless_planets = planet_generation()
Matrix = matrix_gen()
#_______________________________________________________

# Тут читается файл с генерационными именами
with open("Name_gen.txt", "r") as file:
    objects = file.read().split(",")

planetary_reactor = {}

# Подсчет общего количества объектов
total_objects = len(Populated_planets[0]) + len(Lifeless_planets[1]) # Здесь попеременно выводятся значения первого и второго возвращенного значения по индексу

# Создание генерального словаря
for _ in range(total_objects):
    key = random.choice(objects).strip()
    if random.random() < 0.5:
        value = random.choice(Populated_planets[0])
    else:
        value = random.choice(Lifeless_planets[1])
    planetary_reactor[key] = value
#________________________________________________________
# Создание имени файла
filename = f"Planetarium_id_" + str(random.randint(100, 999)) + ".json"

# Сохранение в формате JSON
with open(filename, "w") as file:
    json.dump(planetary_reactor, file)

print(f"Dictionary saved to {filename}")